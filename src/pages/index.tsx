import { Dialog, Spinner, TextInputField } from "evergreen-ui";
import Head from "next/head";
import { useState } from "react";
import axios from "axios";
import Script from "next/script";

export default function Home() {
  const [state, setState] = useState<any>({
    isSubscribed: false,
    showDialogSubscribe: false,
    formData: {},
    formDisabled: false,
  });

  const onChangeData = (key: string, value: any) => {
    setState({
      ...state,
      formData: {
        ...state.formData,
        [key]: value,
      },
    });
  };

  const onClickSubscribe = async () => {
    try {
      setState({ ...state, formDisabled: true });
      await axios.post("/api/subscribe", state.formData);
      setState({
        ...state,
        isSubscribed: true,
        showDialogSubscribe: false,
        formDisabled: false,
        formData: {},
      });
    } catch (e: any) {
      const error: string = e.response
        ? e.response.data.error
        : "Something went wrong. Please try again";
      setState({ ...state, formDisabled: false });
      alert(error);
    }
  };

  return (
    <div>
      <Head>
        <title>Hivee</title>
      </Head>

      <main>
        <div className="w-full h-screen flex justify-center items-center">
          {!state.isSubscribed && (
            <div style={{ maxWidth: 500 }}>
              <div className="px-5 md:px-0 space-y-5 mb-10">
                <div className="uppercase font-bold text-center tracking-wider">
                  Hivee
                </div>
                <div className="font-bold text-4xl text-center">
                  Empower developers to build more products,{" "}
                  <span className="bg-hivee-yellow px-2">faster</span>!
                </div>
                <div className="text-sm text-center md:px-8">
                  Hivee provides a comprehensive suite of tools to help
                  developers streamline their development process, collaborate
                  with colleagues, and build{" "}
                  <span className="bg-hivee-yellow px-1 font-bold">
                    better products
                  </span>
                  .
                </div>
              </div>

              <div className="px-5 md:px-0 space-y-5">
                <div className="text-md text-center px-5">
                  Don&apos;t miss out on our{" "}
                  <span className="bg-hivee-yellow px-1 font-bold">
                    upcoming product launch
                  </span>
                  !
                  <br />
                  Subscribe now to get{" "}
                  <span className="bg-hivee-yellow px-1 font-bold">
                    exclusive early access
                  </span>{" "}
                  and stay up to date on all the new product details. Hurry,
                  this limited-time offer won&apos;t last long!
                </div>

                <div className="flex justify-center">
                  <div
                    className="bg-black text-white px-10 py-3 font-bold border border-black rounded cursor-pointer hover:bg-white hover:text-black"
                    onClick={() =>
                      setState({ ...state, showDialogSubscribe: true })
                    }
                  >
                    I wanna try Hivee!
                  </div>
                </div>
              </div>
            </div>
          )}

          {state.isSubscribed && (
            <div className="space-y-10" style={{ maxWidth: 500 }}>
              <div className="text-center px-5 md:px-0">
                Thank you for subscribing. We are delighted to have you as a
                part of the community. We look forward to sharing our journey
                with you.
              </div>

              <div className="uppercase font-bold text-center tracking-wider">
                Hivee
              </div>
            </div>
          )}
        </div>

        <Dialog
          title="Get Hivee Latest Updates"
          isShown={state.showDialogSubscribe}
          onCloseComplete={() =>
            setState({ ...state, showDialogSubscribe: false })
          }
          hasFooter={false}
        >
          <TextInputField
            label="Your Full Name"
            type="text"
            disabled={state.formDisabled}
            onChange={(e: any) => onChangeData("fullname", e.target.value)}
          />
          <TextInputField
            label="Your Email Adress"
            type="email"
            disabled={state.formDisabled}
            onChange={(e: any) => onChangeData("email", e.target.value)}
          />
          <div className="flex justify-center mb-5">
            {!state.formDisabled && (
              <div
                className="mb-5 bg-black text-white text-center px-10 py-2 border border-black rounded font-bold cursor-pointer hover:bg-white hover:text-black"
                onClick={onClickSubscribe}
              >
                Subscribe
              </div>
            )}
            {state.formDisabled && <Spinner size={30} />}
          </div>
        </Dialog>
      </main>

      <Script
        src="https://www.googletagmanager.com/gtag/js?id=G-D5E17WDSM6"
        strategy="afterInteractive"
      />
      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-D5E17WDSM6');
        `}
      </Script>
    </div>
  );
}
