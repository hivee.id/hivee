import { INotion, Notion } from "@hivee-plugins/notion";
import type { NextApiRequest, NextApiResponse } from "next";

type ApiResponse = {
  error?: string;
  success: boolean;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ApiResponse>
) {
  if (!req.body.fullname || !req.body.email) {
    return res.status(400).json({
      error: "Invalid fullname or email address.",
      success: false,
    });
  }

  const { fullname, email }: any = req.body;
  try {
    const notion: INotion = new Notion({
      secretToken: process.env.NOTION_TOKEN! || "",
    });
    const payload: any = [
      { type: "title", key: "Full Name", value: fullname },
      { type: "email", key: "Email", value: email },
    ];
    await notion
      .db()
      .insert(process.env.NOTION_SUBSCRIBER_EARLY_ACCESS_DB_ID!, payload);

    res.status(200).json({ success: true });
  } catch (e) {
    console.dir(e);
    res.status(500).json({
      error: "Something went wrong. Please try again later.",
      success: false,
    });
  }
}
